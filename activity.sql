USE music_db;

SELECT * FROM artists WHERE name LIKE "%d%";

SELECT * FROM songs WHERE length < 230;

SELECT name, title, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE albums.name LIKE "%a%";

SELECT * FROM albums
    ORDER BY name DESC
    LIMIT 4;

SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY name DESC, title ASC;
