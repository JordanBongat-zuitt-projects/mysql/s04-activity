USE music_db;

INSERT INTO artists (name)
    VALUES ("Taylor Swift"),
    ("Lady Gaga"),
    ("Justin Bieber"),
    ("Ariana Grande"),
    ("Bruno Mars");

INSERT INTO albums (name, year, artist_id)
    VALUE ("Fearless", "2008-01-01", 1),
    ("Red", "2012-01-01", 1),
    ("A Star is Born", "2018-01-01", 2),
    ("Born This Way", "2011-01-01", 2),
    ("Purpose", "2015-01-01", 3),
    ("Believe", "2012-01-01", 3),
    ("Dangerous Women", "2016-01-01", 4),
    ("Thank U, Next", "2019-01-01", 4),
    ("24k Magic", "2016-01-01", 5),
    ("Earth to Mars", "2011-01-01", 5);

INSERT INTO songs (title, length, genre, album_id) VALUES
    ("Fearless", 246, "Pop rock", 1),
    ("Love Story", 213, "Country pop", 1),
    ("State of Grace", 213, "Alternative Rock", 2),
    ("Red", 204, "Country", 2),
    ("Black Eyes", 181, "Rock and roll", 3),
    ("Shallow", 201, "Country, rock, folk rock", 3),
    ("Born This Way", 252, "Electropop", 4),
    ("Sorry", 192, "Dancehall-poptropical housemoombahton", 5),
    ("Boyfriend", 251, "Pop", 6),
    ("Into You", 242, "EDM house", 7),
    ("Thank U Next", 196, "Pop, R&B", 8),
    ("24K Magic", 207, "Funk, disco, R&B", 9),
    ("Lost", 192, "Pop", 10);
